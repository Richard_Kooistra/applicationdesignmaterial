package nl.bioinf.junitdemos;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 * Created by michiel on 14/03/2017.
 */
public class TextUtilsTest {

    @Test
    public void getLongestWordSunny() {
        final String longestWordSunny = "de kat krabt de krullen van de trap";
        final String expectedLongest = "krullen";
        final String givenLongest = TextUtils.getLongestWord(longestWordSunny);
        assertEquals(expectedLongest, givenLongest);
    }

    @Test
    public void getLongestWordTie() {
        final String longestWordTie = "de kat krabt de krullen krallen krollen van de trap";
        final String expectedLongest = "krollen";
        final String givenLongest = TextUtils.getLongestWord(longestWordTie);
        assertEquals(expectedLongest, givenLongest);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getLongestWordNull() {
        final String nullInput = null;
        final String givenLongest = TextUtils.getLongestWord(nullInput);
    }

    @Test
    public void getLongestWordPunctuation() {
        final String punctuated = "de kat krabt, maar soms.";
        final String expected = "krabt";
        final String givenLongest = TextUtils.getLongestWord(punctuated);
        assertEquals(expected, givenLongest);
    }

    @Test
    public void stripPunctuationTest() {
        final String input = "de kat krabt, maar soms.";
        final String expected = "de kat krabt maar soms";
        final String returned = TextUtils.stripPunctuation(input);
        assertEquals(expected, returned);
    }

    @Test
    public void testDoubleEquals() {
        double one = 0.234566;
        double two = 0.234567;

        assertEquals(one, two, 0.00001);

    }

    @Test
    public void testEquals() {
        final MyObj one = new MyObj(33, "foo");
        final MyObj two = new MyObj(33, "foo");
        assertEquals(one, two);
    }

    @Test
    @Ignore
    public void testSame() {
        final MyObj one = new MyObj(33, "foo");
        final MyObj two = new MyObj(33, "foo");
        assertSame(one, two);
    }

}