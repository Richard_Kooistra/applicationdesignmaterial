package nl.bioinf.junitdemos;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by michiel on 15/03/2017.
 */
@RunWith(Parameterized.class)
public class AdditionTest {
    private int expected;
    private int first;
    private int second;

    public AdditionTest(int expectedResult,
                        int firstNumber,
                        int secondNumber) {
        this.expected = expectedResult;
        this.first = firstNumber;
        this.second = secondNumber;
    }

    @Parameterized.Parameters
    public static Collection<Integer[]> addedNumbers() {
        return Arrays.asList(
                new Integer[][]{
                        {3, 1, 2},
                        {5, 2, 3},
                        {7, 3, 4},
                        {9, 4, 5}
                });
    }

    @Test
    public void sum() {
        Addition add = new Addition();
        System.out.println("adding " + first + " + " + second + "; expected result=" + expected);
        assertEquals(expected, add.addNumbers(first, second));
    }

    @Test
    public void print() {
        System.out.println("expected = " + expected);
    }

    @Test
    public void testSubtract() {
        System.out.println("testing subtract");
        Addition add = new Addition();
        add.subtract(1, 2);
    }

}