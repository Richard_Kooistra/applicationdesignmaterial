package nl.bioinf.junit_exercise;

import org.junit.Test;

import java.lang.reflect.Executable;
import java.util.List;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PrimerTest {

    private static final double DELTA = 1e-4;

    @Test
    public void getGcPercentageSunny() {
        final String sequenceSunny = "ATCGATCGATCGATCG";
        final double expectedGcPercentage = 50.0;
        Primer p = new Primer(sequenceSunny);
        double givenGcPercentage = p.getGcPercentage();
        assertEquals(expectedGcPercentage, givenGcPercentage, DELTA);
    }

    @Test
    public void getGcPercentageNull() {
        try {
            Primer p = new Primer(null);
        } catch (IllegalArgumentException e) {
            assertEquals(Primer.CONSTRUCTOR_WARNING, e.getMessage());
        }
    }

    @Test
    public void getGcPercentageNumbersInSequence() {
        final String numbersInSequence = "ATCGATCG5874";
        try {
            Primer p = new Primer(numbersInSequence);
        } catch (IllegalArgumentException e) {
            assertEquals(Primer.CONSTRUCTOR_WARNING, e.getMessage());
        }
    }


    @Test
     public void getMeltingTemperatureSunny() {
        final String sequenceSunny = "ATCGATCGATCG";
        final double expectedMeltingTemperature = 36.0;
        Primer p = new Primer(sequenceSunny);
        double givenMeltingTemperature = p.getMeltingTemperature();
        assertEquals(expectedMeltingTemperature, givenMeltingTemperature, DELTA);
    }

    @Test
    public void getMeltingTemperatureNull() {
        try {
            Primer p = new Primer(null);
        } catch (IllegalArgumentException e) {
            assertEquals(Primer.CONSTRUCTOR_WARNING, e.getMessage());
        }
    }

    @Test
    public void getMeltingTemperatureNumbersInSequence() {
        final String numbersInSequence = "ATCGATCG5874";
        try {
            Primer p = new Primer(numbersInSequence);
        } catch (IllegalArgumentException e) {
            assertEquals(Primer.CONSTRUCTOR_WARNING, e.getMessage());
        }
    }

    @Test
    public void createRandomPrimer() {
        int expectedLenth = 20;
        Primer randomPrimer = Primer.createRandomPrimer(expectedLenth);
        assertEquals(expectedLenth, randomPrimer.getLength());
    }

    @Test
    public void createRandomPrimerNegativeLength() {
        int requestedLength = -10;
        assertThrows(IllegalArgumentException.class, () -> Primer.createRandomPrimer(requestedLength));
    }

    @Test
    public void testFilterRandomPrimerJava8() {
        List<Primer>testCollection = Primer.createPrimerCollection(20);
        testCollection.stream()
                .peek(System.out::println)
                .filter(p -> p.getMeltingTemperature() > 50 && p.getMeltingTemperature() <= 60)
                .filter(this::containsALLFourNucleotides)
                .forEach(p -> System.out.println(p.getGcPercentage() + " PASSED" ));

    }

    private boolean containsALLFourNucleotides(Primer p) {
        String sequence = p.getSequence();
        return sequence.contains("A")
                && sequence.contains("C")
                && sequence.contains("G")
                && sequence.contains("T");
    }


}
