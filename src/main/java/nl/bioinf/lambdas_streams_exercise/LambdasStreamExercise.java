package nl.bioinf.lambdas_streams_exercise;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Creation date: 7-2-2018
 *
 * @author Michiel Noback (&copy; 2018)
 * @version 0.01
 */
public class LambdasStreamExercise {
    public static final String DNA_ONE = "GTATTCATAC";
    public static final String DNA_TWO = "CATCATGAAATCGCTTGTCGCACTACTGCTGCTTTTAGTCGCTACTTCTGCCTTTGCTGACCAGTATGTAAATGGCTCTGGAGTCCTCTCCTTGCCAAA";
    public static final String DNA_THREE = "CATCATGAAATCGCTTGTCGCAYTACTGCTGCTTTTANTCGCTACTTCTGCCTTTGCTGACCKAGTATGTAAATGGCTCTGGAGTCCTCTCCTTGWCCAAA";



    public static String dnaToAbbreviatedNames(String DNA) {
        return null;
    }

    public static double dnaToWeight(String DNA) {
        return 0;
    }

    public static int countNucleotide(String DNA, char nucleotide) {
        return 0;
    }
}
