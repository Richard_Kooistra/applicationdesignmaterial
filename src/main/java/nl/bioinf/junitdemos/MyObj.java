package nl.bioinf.junitdemos;

/**
 * Created by michiel on 15/03/2017.
 */
public class MyObj {
    private int number;
    private String name;

    public MyObj(int number, String name) {
        this.number = number;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyObj myObj = (MyObj) o;

        if (number != myObj.number) return false;
        return name.equals(myObj.name);
    }

    @Override
    public int hashCode() {
        int result = number;
        result = 31 * result + name.hashCode();
        return result;
    }
}
