package nl.bioinf.junitdemos;

public class TextUtils {
    /**
     * Finds the longest word in a given string.
     * If there is a tie, the last found longest
     * is returned.
     *
     * @param text the text to scan
     * @return longestWord
     * @throws IllegalArgumentException ex if text is null
     */
    public static String getLongestWord(String text) {
        if (null == text) {
            throw new IllegalArgumentException("text cannot be null");
        }

        text = stripPunctuation(text);
        String[] words = text.split(" ");
        String longest = "";
        for (String word : words) {
            if (word.length() >= longest.length()) {
                longest = word;
            }
        }
        return longest;
    }

    public static String stripPunctuation(String text) {
        //stripping punctuation

        return text.replaceAll("[,.]", "");
    }
}
