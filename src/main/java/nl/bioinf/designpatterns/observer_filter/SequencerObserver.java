package nl.bioinf.designpatterns.observer_filter;

/**
 * Created by michiel on 07/03/2017.
 */
public interface SequencerObserver {
    /**
     * will get notified here when a new sequence has been determined/finsihed.
     *
     * @param sequence
     */
    public void newSequenceDetermined(Sequence sequence);
}
