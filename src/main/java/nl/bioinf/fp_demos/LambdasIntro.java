package nl.bioinf.fp_demos;

//import javafx.scene.control.Button;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LambdasIntro {
    public static void main(String[] args) {

        JButton b = new JButton();
        b.addActionListener((event) -> System.out.println(event.getActionCommand()));
        b.doClick();
        //.setOnAction((event) -> System.out.println(event.getEventType()));

        Comparator<String> comp;
        comp = (String first, String second) -> Integer.compare(first.length(), second.length());
        //Same as
        comp = (first, second) -> Integer.compare(first.length(), second.length());
        //same as
        comp = (first, second) -> {
            return Integer.compare(first.length(), second.length());
        };
        //Same as
        comp = new Comparator<String>() {
            @Override
            public int compare(String first, String second) {
                return Integer.compare(first.length(), second.length());
            }
        };

        //call it
        System.out.println("comp = " + comp.compare("foo", "barr"));
//
//        List<String> l = new ArrayList<>();
//        l.add("foo");
//        l.add("bar");
//        l.add("drool");
//        l.add("gooish");
//        l.add("ha");
//        //Java 9: List.of("foo", "bar", "drool", "gooish", "ha");
//
//        Collections.sort(l, comp);
//        System.out.println("l.toString() = " + l.toString());



    }
}
